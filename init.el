;; init.el --- Emacs configuration

;; INSTALL PACKAGES
;; --------------------------------------

(require 'package)

;(add-to-list 'package-archives
;	     '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar myPackages
  '(better-defaults
    material-theme
    neotree
    tabbar
    elpy
    magit))

(mapc #'(lambda (package)
    (unless (package-installed-p package)
      (package-install package)))
      myPackages)

;; BASIC CUSTOMIZATION
;; --------------------------------------

(setq inhibit-startup-message t)
(load-theme 'material t)
(global-linum-mode t)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(setq column-number-mode t)
(set-face-attribute 'default nil :height 80)
(tabbar-mode 1)
(elpy-enable)

;; Configure newtree
;; --------------------------------------

(require 'neotree)
(global-set-key [f8] 'neotree-toggle)


;; Configure Magit
;; --------------------------------------

(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-x M-g") 'magit-dispatch-popup)

;; Disable pesky features that Ubuntu and Raspbian have enabled by default.
;; ------------------------------------------------------------------------------

;; Disable copy on select
(fset 'evil-visual-update-x-selection 'ignore)
(setq select-active-regions nil)
(setq mouse-drag-copy-region nil)
(setq x-select-enable-primary nil)

;; disable ido
(ido-mode -1)

;; Set up transparency effets
;; ------------------------------------------------------------------------------

(defun toggle-transparency ()
  (interactive)
  (let ((alpha (frame-parameter nil 'alpha)))
    (set-frame-parameter
     nil 'alpha
     (if (eql (cond ((numberp alpha) alpha)
                    ((numberp (cdr alpha)) (cdr alpha))
                    ;; Also handle undocumented (<active> <inactive>) form.
                    ((numberp (cadr alpha)) (cadr alpha)))
              100)
         '(85 . 50) '(100 . 100)))))

;; Set transparency of emacs
(defun transparency (value)
  "Sets the transparency of the frame window. 0=transparent/100=opaque"
  (interactive "nTransparency Value 0 - 100 opaque:")
  (set-frame-parameter (selected-frame) 'alpha value))

(transparency 85)

(global-set-key (kbd "C-c t") 'toggle-transparency)


;; Configure window navigation
;; --------------------------------------

(defun window-switch-above ()
  (interactive)
  (set-frame-selected-window (selected-frame) (window-in-direction 'above)))
(defun window-switch-below ()
  (interactive)
  (set-frame-selected-window (selected-frame) (window-in-direction 'below)))
(defun window-switch-left ()
  (interactive)
  (set-frame-selected-window (selected-frame) (window-in-direction 'left)))
(defun window-switch-right ()
  (interactive)
  (set-frame-selected-window (selected-frame) (window-in-direction 'right)))

(global-set-key (kbd "C-x C-2") 'window-switch-below)
(global-set-key (kbd "C-x C-4") 'window-switch-left)
(global-set-key (kbd "C-x C-6") 'window-switch-right)
(global-set-key (kbd "C-x C-8") 'window-switch-above)

;; Tramp configuration
;; --------------------------------------

;; Set tramp to use auto-mux in order to connect to existing sessions
;; This requires the following in ~/.ssh/config:
;;
;; Host *
;;     ControlMaster auto
;;     ControlPath ~/.ssh/%r@%h:%p

(customize-set-variable
 'tramp-ssh-controlmaster-options
 (concat
  "-o ControlPath=~/.ssh/%%r@%%h:%%p "
  "-o ControlMaster=auto -o ControlPersist=yes"))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (dash-functional esxml tabbar neotree material-theme magit elpy better-defaults))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Python hooks
;; --------------------------------------

(add-to-list 'load-path "~/.emacs.d/lisp/")
(require 'fill-column-indicator)
(setq fci-rule-column 100)
(setq fci-rule-color "#b7c7cf")
(fci-mode)

(add-hook 'python-mode-hook
          '(lambda ()
             (fci-mode)
             )
          t)
